# Vegano Foods - Software Development Challenge

The Software Development Challenge is part of the evaluation steps for a technical role at Vegano Foods. The challenge must be completed and submitted on time defined with each candidate and the Vegano's Dev Team. Also, the challenge will be part of the onsite interview process.

You are allowed to use whatever programming language, framework, and database you feel the most comfortable with.

Feel free to email dev@veganofoods.com if you have any questions about the challenge.

## Challenge Response

Howdy,

This was kind of fun. The given endpoint doesn't accept CSV formatted data and I had to spend some time troubleshooting a pretty mysterious error message.

Eventually figured out how to transform the CSV data into something workable, but unfortunately this insight came late and is causing a bug.

That bug is: to update the sync_status field on the product, I need to reference a PostgresQL row by a JSON field, and I lose that reference while cleaning the data before communicating with Shopify. Therefore, even on a successful shopify response, we don't see the sync_status update.

There's also a race condition causing the details of the sync to not display in the devtools console, could probably be solved by extracting the code on lines 64-67 in api/shopifysync.js to a named function.

## Testing instructions

Pull the project, run npm i to initialize next and install my helper packages (sass, csvparser, etc).

Upload products under the `upload products` view, check the products under `Review Uploaded Products`, and initiate sync with `Sync with Shopify`.

## Challenge Description

In this challenge, the main goals are:

- Develop an API that will be responsible for:
  1. An endpoint that handles uploading a product csv file
     - The product csv file sample template is available here: https://gitlab.com/vegano-challenges/full-stack-challenge/-/blob/main/product_template.csv
     - Read a product csv file, validate it and save the data into the database with a sync_status equal to `not synced`
  2. An endpoint that handles retrieving the products list
  3. And endpoint that handles creating the products on Shopify (references bellow)
     - Call the Shopify Product API and create all products with sync_status = `not synced`
     - Update the product sync_status to `synced` if the response from Shopify is ok, else update to `failed`
- Develop a front-end page where the user can:
  1. Upload a product csv file
  2. List the products from the database (just need to show title, vendor, variant price, image src, and sync status)
  3. Sync all products with sync_status = `not synced`

You can assume that:

- The csv columns will always be in that order.
- The only data needed is the data you can see in the csv template file. All columns that are blank are there because of the Shopify requirement.

## Reference

- Shopify Product CSV: https://help.shopify.com/en/manual/products/import-export/using-csv#product-csv-file-format
- Shopify Product API: https://shopify.dev/api/admin/rest/reference/products/product#create-2021-07

### Example of how to create a new product using Shopify API:

```POST /admin/api/2021-07/products.json
{
  "product": {
    "title": "Fender Jazz Bass",
    "body_html": "",
    "vendor": "Fender",
    "product_type": "Bass",
    "tags": [
      "4-strings"
    ]
  }
}
```

### URL to access the Shopify Product API Endpoint:

`https://dffe70589b280740b28560b4a1101b4b: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@vegano-challenge.myshopify.com/admin/api/2021-07/products.json`

## Submission Instructions

1. Fork this project on GitLab. You will need to create an account if you don't already have one.
2. Complete the challenge as described above using your fork.
3. Add the instructions on how to build/run your application.
4. Commit and push all your changes to your fork on GitLab and submit a pull request (PR).
5. Email dev@veganofoods.com with the PR link to let Vegano's Dev Team know you have submitted your challenge.
