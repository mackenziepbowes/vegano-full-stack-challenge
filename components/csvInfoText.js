import styles from './csvInfoText.module.scss';

export const CSVInfoText = () => {
	return (
		<>
			<p>Products must be a CSV with the following headers in this order:</p>

			<div className={styles.headerGrid}>
				<div>Handle</div>
				<div>Title</div>
				<div className={styles.optional}>Body (HTML)</div>
				<div>Vendor</div>
				<div>Type</div>
				<div>Tags</div>
				<div>Published</div>
				<div>Option1 Name</div>
				<div>Option1 Value</div>
				<div className={styles.optional}>Option2 Name</div>
				<div className={styles.optional}>Option2 Value</div>
				<div className={styles.optional}>Option3 Name</div>
				<div className={styles.optional}>Option3 Value</div>
				<div className={styles.optional}>Variant SKU</div>
				<div>Variant Grams</div>
				<div className={styles.optional}>Variant Inventory Tracker</div>
				<div className={styles.optional}>Variant Inventory Quantity</div>
				<div>Variant Inventory Policy</div>
				<div>Variant Fulfillment Service</div>
				<div>Variant Price</div>
				<div className={styles.optional}>Variant Compare At Price</div>
				<div className={styles.optional}>Variant Requires Shipping</div>
				<div className={styles.optional}>Variant Taxable</div>
				<div className={styles.optional}>Variant Barcode</div>
				<div>Image Src</div>
				<div className={styles.optional}>Image Position</div>
				<div className={styles.optional}>Image Alt Text</div>
				<div className={styles.optional}>Gift Card</div>
				<div>Status</div>
			</div>
			<p className={styles.optional}>Green background denotes OPTIONAL data, but the file MUST have this header</p>
		</>
	)
}