export const GetProducts = async () => {

	const url = 'http://localhost:3000/api/csvdb';

	const request = {
		method: 'GET',
		mode: 'cors',
		cache: 'no-cache',
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const res = await fetch(url, request)
		.then(response => response.json());
	return res;
}