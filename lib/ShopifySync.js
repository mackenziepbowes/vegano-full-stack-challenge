export const ShopifySync = async () => {

	const url = 'http://localhost:3000/api/shopifysync';

	const request = {
		method: 'POST',
		mode: 'cors',
		cache: 'no-cache',
		headers: {
			'Content-Type': 'application/json'
		},
		//body
	};

	const res = await fetch(url, request)
		.then(response => response.json());
	return res;
}