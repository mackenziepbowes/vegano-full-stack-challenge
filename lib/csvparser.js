// A script for Parsing and Accepting/Rejecting CSV files for this project

const parse = require('csv-parse/lib/sync');

export const readCSV = async (file) => {
	const result = parse(file);
	// Result is an array of strings
	let response; // to send back to the fe
	let data; // JSON objects of the products
	const expectedHeaders = 'Handle,Title,Body (HTML),Vendor,Type,Tags,Published,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,Variant SKU,Variant Grams,Variant Inventory Tracker,Variant Inventory Qty,Variant Inventory Policy,Variant Fulfillment Service,Variant Price,Variant Compare At Price,Variant Requires Shipping,Variant Taxable,Variant Barcode,Image Src,Image Position,Image Alt Text,Gift Card,Status';
	if (result[0].toString() !== expectedHeaders) {
		response = { status: 'error', message: 'Malformed CSV' };
		return response;
	}
	if (result[0].toString() === expectedHeaders) {
		data = parse(file, { columns: true });
		response = { status: 'success', data };
		return response;
	}
	response = { status: 'error', message: 'Function failed to parse file' };
	return response;
};