export const PostCSV = async (data) => {

	const body = JSON.stringify(data);

	const url = 'http://localhost:3000/api/csvdb';

	const request = {
		method: 'POST',
		mode: 'cors',
		cache: 'no-cache',
		headers: {
			'Content-Type': 'application/json'
		},
		body
	};

	const res = await fetch(url, request)
		.then(response => response.json());
	return res;
}