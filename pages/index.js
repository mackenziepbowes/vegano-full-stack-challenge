// UI for the following operations:
// 1) Upload a CSV of items to a db
// 1a) Some info regarding 'proper' CSV formatting
// 1b) get feedback if the CSV is incorrectly formatted
// 2) Retrieve a list of items from a db
// 2a) Show title, vendor, variant price, image src, and sync status
// 3) A button to sync items to Shopify

import { useState, useEffect } from 'react';

import styles from '../styles/Home.module.scss';
import { CSVInfoText } from '../components/csvInfoText';
import { readCSV } from '../lib/csvparser';
import { PostCSV } from '../lib/PostCSV';
import { GetProducts } from '../lib/GetProducts';
import { ShopifySync } from '../lib/ShopifySync';

export default function Home() {

	const [message, setMessage] = useState('');

	// control the UI state ie: show upload screen, show db response, show shopify response
	const [state, setState] = useState('initial'); //todo: rename to a better variable

	const [status, setStatus] = useState(''); // a variable to color API response messages

	const [products, setProducts] = useState(); // trigger a rerender when product list is populated

	const readFile = e => {
		const reader = new FileReader();
		const file = e.target.files[0];
		reader.addEventListener('load', async (event) => {
			const file = event.target.result;
			const result = await readCSV(file);
			if (result.status !== 'error') {
				setMessage('CSV Parsed correctly, uploading...');
				const dbresponse = await PostCSV(result.data);
				setStatus(dbresponse.status);
				setMessage(dbresponse.message);
			}
			if (result.status === 'error') {
				setMessage('CSV Invalid');
			}
		});
		reader.readAsText(file);
	}

	const uploadProducts = () => {
		setState('upload');
		setMessage('');
	}

	const reviewProducts = async () => {
		setState('review');
		setProducts(''); // React requires changing the data type of state variables to trigger a rerender, passing empty string here to later pass an array
		setMessage('Downloading Products...');
		const productResponse = await GetProducts();
		if (productResponse.status === 'success') {
			const productArray = productResponse.data.map(instance => instance); // names are getting confusing :( 'instance' is each instance of a product in an array, the array looks like [{product: {// rest of json}}];
			setProducts(productArray); // triggers a rerender :) 
			setMessage('');
		}

	}

	const syncShopifyProducts = async () => {
		setState('syncing');
		setMessage('Syncing with Shopify...');
		const response = await ShopifySync();
		console.log(response);
		if (typeof response.message !== 'undefined') {
			setMessage(response.message);
		}
	}

	return (
		<div className={styles.indexBody}>
			{
				(state !== 'initial') ?
					<div className={styles.buttonContainer}>
						<button onClick={uploadProducts}>Upload Products</button>
						<button onClick={reviewProducts}>Review Uploaded Products</button>
						<button onClick={syncShopifyProducts}>Sync with Shopify</button>
					</div>
					: null
			}

			{
				(state === 'initial') ?
					<>
						<InitialState stateFunction={setState} />
						<div className={styles.buttonContainer}>
							<button onClick={uploadProducts}>Upload Products</button>
							<button onClick={reviewProducts}>Review Uploaded Products</button>
							<button onClick={syncShopifyProducts}>Sync with Shopify</button>
						</div>
					</>
					: null
			}
			{
				(state === 'upload') ?
					<UploadState message={message} uploadFunction={readFile} messageStatus={status} />
					: null
			}
			{
				(state === 'review') ?
					<ReviewState message={message} products={products} />
					: null
			}
			{
				(state === 'syncing') ?
					<SyncState message={message} />
					: null
			}
		</div>
	)
}


const InitialState = () => {
	return (
		<>
			<h1>Select A Task</h1>
		</>
	)
}

const UploadState = (props) => {
	const { message, uploadFunction, messageStatus } = props;

	let style = { backgroundColor: `rgba(0,0,0,0)` };

	if (messageStatus === 'success') {
		style = { backgroundColor: `rgba(100,250,100,0.25)` };
	}

	if (messageStatus === 'error') {
		style = { backgroundColor: `rgba(250,100,100,0.25)` };
	}

	return (
		<>
			<h1>Upload Products</h1>
			<div className={styles.fileInputContainer}>
				<input type="file" accept=".csv" onChange={uploadFunction}></input>
			</div>
			<br />
			{
				(message !== '')
					? <h2 className={styles.apiStatus} style={style}>{message}</h2>
					: <CSVInfoText />
			}
		</>
	)
}

const ReviewState = (props) => {
	const { message, products } = props;
	return (
		<>
			<h1>Review Products</h1>
			{
				(message !== '')
					? <h2>{message}</h2>
					: null
			}

			{
				(products !== '')
					?
					<>
						<div className={styles.productGrid}>
							{products.map(product => {
								return (
									<div className={styles.gridItem}>
										{Object.keys(product.product).map(field => {
											if (field === 'Title' || field === 'Vendor' || field === 'Variant Price' || field === 'Image Src') {
												return (
													<>
														<div>{field}: </div>
														<div> {product.product[field]}</div>
													</>
												)
											}
										})}
										<div>Sync Status: </div>
										<div>{product.sync_status}</div>
									</div>
								)
							})}
						</div>
					</>
					: null

			}
		</>
	)
}

const SyncState = (props) => {
	const { message } = props;

	return (
		<>
			<h1>Sync with Shopify</h1>
			{(message !== '')
				? <h2>{message}</h2>
				: null}
		</>
	)
}