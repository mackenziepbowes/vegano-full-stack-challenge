// This is an api endpoint accessible at <domain.tld>/api/csvdb
// It takes a POST and GET request
// POST requests require CSVs, formatted correctly, and stores them in a db
// GET requests read out the db's info

import { createClient } from '@supabase/supabase-js';

const URL = process.env.SUPABASE_URL;
const KEY = process.env.SUPABASE_KEY;

const supabase = createClient(URL, KEY);

export default async function handler(req, res) {
	if (req.method === 'POST') {
		try {
			const products = req.body.map(product => {
				return { product, sync_status: 'not synced' };
			})

			const { data, error, status } = await supabase
				.from('Products')
				.insert(products);

			if (error && status !== 406) {
				throw error;
			}

			if (data) {
				res.status(200).json({ status: 'success', message: 'Upload OK' });
			}

		} catch (error) {
			res.status(500).json({ status: 'error', message: error.message });
		}
	}
	if (req.method === 'GET') {
		try {
			let { data, error, status } = await supabase
				.from('Products')
				.select('product,sync_status');

			if (error && status !== 406) {
				throw error;
			}

			if (data) {
				res.status(200).json({ status: 'success', data });
			}

		} catch (error) {
			res.status(500).json({ status: 'error', message: error.message });
		}

	}
	if (req.method !== 'POST' && req.method !== 'GET') {
		res.status(405).json({ message: 'That method is not allowed on this endpoint.' });
	}
}





