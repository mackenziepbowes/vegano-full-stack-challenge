// This is an api endpoint accessible at <domain.tld>/api/shopifysync
// It takes a POST request only

// Connect to product db
import { createClient } from '@supabase/supabase-js';

const URL = process.env.SUPABASE_URL;
const KEY = process.env.SUPABASE_KEY;

const supabase = createClient(URL, KEY);

export default async function handler(req, res) {
	if (req.method === 'POST') {
		try {
			// Get all products where 'sync_status' == 'not synced'
			// NOTE: the above instruction is a logic error - later in this function,
			// we set products that failed to sync to a sync_status of 'failed'.
			// We shouldn't prevent the user from attempting to sync the product again, 
			// who knows why the sync failed.
			const products = await GetUnsyncedProducts();

			// arrays to handle responses on each product
			let responsePackage = [];
			// may produce an empty array if none of the products are not synced


			// I can't work on this anymore, but somehow this map function is mutating
			// the 'products' variable declared on line 20... or something
			// This is an issue because I need to reference a given index of that object
			// in order to select the correct row for updating in my database.

			// To fix this, the ideal solution is to format the JSON data to match
			// Shopify's requirements BEFORE saving it to the db so I don't have to 
			// have the following cleaning function. 

			const productsToSync = products.map(product => {
				// allows for 'failed' sync statuses to attempt a sync
				if (product.sync_status !== 'synced') {
					let cleanedProduct = product;
					//CSV fields don't match JSON fields
					cleanedProduct.product['title'] = product.product['Title'];
					cleanedProduct.product['tags'] = product.product['Tags'];
					cleanedProduct.product['product_type'] = product.product['Type'];
					cleanedProduct.product['image'] = product.product['Image Src'];
					cleanedProduct.product['vendor'] = product.product['Vendor'];
					cleanedProduct.product['grams'] = product.product['Variant Grams']
					cleanedProduct.product['presentment_prices'] = [
						{
							"price": {
								"amount": `${product.product['Variant Price'] / 100}`,
								"currency_code": "USD"
							},
							"compare_at_price": product.product['Compare At Price']
						}
					];
					return cleanedProduct;
				}
			});

			// Call Shopify API and create new products from products where 'sync_status' == 'not synced'

			// make sure the array isn't empty
			if (typeof productsToSync[0] !== 'undefined') {
				productsToSync.forEach(async (product, index) => {
					// Folded into a function declared later in the document for devex/preventing race conditions
					responsePackage[index] = await HandleShopifyResponse(product); // should be a 'referenceProduct' variable passed as well but that's not working
				}
				);
			}
			// console.log('Response Package Array:', responsePackage);

			// error handling on Response Package
			responsePackage.forEach(response => {
				if (response.ShopifyResponse.status !== 200) {
					throw ShopifyResponse;
				}
				if (response.SupabaseResponse.status !== 200) {
					throw SupabaseResponse;
				}
			});
			// Final Send
			res.status(200).json({ message: 'Products synced.', details: responsePackage });
		}
		catch (error) {
			res.status(error.status).json({ message: 'Sync failed.', details: error.error });
		}
	}
	if (req.method !== 'POST') {
		res.status(405).json({ status: 'error', message: `Request type ${req.method} is an invalid request type at this endpoint.` });
	}
}



const GetUnsyncedProducts = async () => {
	try {
		let { data, error, status } = await supabase
			.from('Products')
			.select('product,sync_status');

		if (error && status !== 406) {
			throw error;
		}

		if (data) {
			return data;
		}

	} catch (error) {
		return { status: '500', message: error.message };
	}
}

const CallShopify = async (data) => {

	const url = 'https://dffe70589b280740b28560b4a1101b4b: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@vegano-challenge.myshopify.com/admin/api/2021-07/products.json';
	const body = JSON.stringify(data);
	const request = {
		method: 'POST',
		mode: 'cors',
		cache: 'no-cache',
		headers: {
			'Content-Type': 'application/json'
		},
		body
	};
	const res = await fetch(url, request)
		.then(response => response.json());
	return res;
}

const UpdateProduct = async (instance, sync_status) => {
	try {
		let { data, error, status } = await supabase
			.from('Products')
			.update({ 'sync_status': sync_status })
			.eq('product', JSON.stringify(instance));
			// unfortunately, json fields don't seem to support 'like', 'contains', 'ilike' etc filters
		if (error && status !== 406) {
			throw error;
		}

		if (data) {
			return { status: 200, data };
		}

	} catch (error) {
		return { status: '500', message: error.message };
	}
}

const HandleShopifyResponse = async (product) => { // should be a 'referenceProduct variable as well

	try {
		const ShopifyResponse = await CallShopify(product);

		// On successful Shopify Response, set 'sync_status' to 'synced' in db
		if (typeof ShopifyResponse.product !== 'undefined') {	
			const SupabaseResponse = await UpdateProduct(product, 'synced'); // this should be referenceProduct
			
			ShopifyResponse.status = '200';
			const ResponsePackage = {
				ShopifyResponse,
				SupabaseResponse
			}
			return ResponsePackage;
		}

		// On unsuccessful Shopify Response, set 'sync_status' to 'failed' in db
		if (typeof ShopifyResponse.errors !== 'undefined') {
			throw ShopifyResponse;
		}
	}
	catch (error) {
		const SupabaseResponse = UpdateProduct(product, 'failed');
		error.status = '500';
		const ResponsePackage = {
			ShopifyResponse: error,
			SupabaseResponse
		}
		return ResponsePackage;
	}

}